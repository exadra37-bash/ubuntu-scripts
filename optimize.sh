#!/bin/bash 
# @author Exadra37 <kfoxtrot37@gmail.com>
# @since 2014/10/26
# @link exadra37.com

command="$1"

# Optimizations for SSD
# https://wiki.debian.org/SSDOptimization
# https://wiki.archlinux.org/index.php/Solid_State_Drives
# https://sites.google.com/site/easylinuxtipsproject/speed#TOC-Make-your-Solid-State-Drive-SSD-run-faster
# http://askubuntu.com/questions/18903/how-to-enable-trim/19480#19480
# benchmark http://ubuntuforums.org/showthread.php?t=1464706
if [[ "ssd" == $command || "all" == $command ]]
	then
		file=/etc/sysctl.d/99-zssd.conf
		printf "\n Optimizing SSD... \n"
		echo "vm.swappiness=1" > sudo $file
		echo "vm.vfs_cache_pressure=50" >> sudo $file

		echo 1 > sudo /sys/block/sdb/queue/iosched/fifo_batch

		# run TRIM on your SSD in every reboot
		file=/etc/rc.local
		sudo sed -i.old 's|exit 0| |g' $file
		echo "fstrim -v /" >> sudo $file
		# If your home directory is not in a separate partition comment the below line
		echo "fstrim -v /home" >> sudo $file 
		echo "exit 0" >> sudo $file

		# by default Ubuntu 14.10 is running TRIM every wekk using a cron job
		# i prefer to run it every day
		sudo mv /etc/cron.weekly/fstrim /etc/cron.daily/fstrim

		file=/etc/fstab

		
		sudo cp $file $file"_original"
		sudo grep -i 'noatime' $file || sed -i 's|errors=|noatime,errors=|g' $file
		sudo grep -i 'noatime' $file || sed -i 's|defaults|noatime,defaults|g' $file

		# apply changes immediately
		#sudo sysctl --system 
fi

