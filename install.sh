#!/bin/bash
# @author Exadra37 <kfoxtrot37@gmail.com>
# @since 2014/10/26
# @link exadra37.com

command="$1"

# 4. Tool to monitor PC.o. better than Top or Gnome Monitor
# http://hisham.hm/htop/
if [[ "htop" == $command || "all" == $command ]]
	then
		printf "\n 4. Installing HTOP Tool \n"
		sudo apt-get -y install htop
fi

# 11. Install Curl
# http://curl.haxx.se/
if [[ "curl" == $command || "all" == $command ]]
	then
		printf "\n 11. Installing CURL"
		sudo apt-get -y install curl
fi

# 12. Install a betther shell using ZSH with OH-MY-ZSH
# http://www.zsh.org/
# http://ohmyz.sh/
if [[ "zsh" == $command || "all" == $command ]]
	then
		printf "\n 12. Install ZSH with OH-MY-ZSH \n"
		sudo apt-get -y install zsh
		curl -L http://install.ohmyz.sh | sh
		zsh
fi

# 9. Install Sublime Text 3 - Very Fast a Powerfull text editor
# http://www.sublimetext.com/3
if [[ "sublime" == $command || "all" == $command ]]
	then
		printf "\n 9. Installing Sublime Text 3 \n"
		cd /tmp
		wget http://c758482.r82.cf2.rackcdn.com/sublime-text_build-3065_amd64.deb
		sudo dpkg -i -R sublime-text_build-3065_amd64.deb
		# symbolic lunk to allow open it from command line
		ln -s /opt/sublime_text/sublime_text /usr/bin/sublime
		rm -rf sublime-text_build-3065_amd64.deb
		cd ~
fi

# 10. Install Google Chrome
# http://askubuntu.com/a/79284
if [[ "chrome" == $command || "all" == $command ]]
	then
		printf "\n 10. Install Chrome \n"
		cd /tmp
		sudo apt-get -y install libxss1 libappindicator1 libindicator7
		wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
		sudo dpkg -i google-chrome*.deb
		rm -rf google-chrome-stable_current_amd64.deb
		cd ~
fi


# 11. Install Skype
# http://www.wikihow.com/Install-Skype-Using-Terminal-on-Ubuntu
if [[ "skype" == $command || "all" == $command ]]
	then
		printf "\n 11. Install Skype \n"
		cd /tmp
		sudo apt-get -y install libqt4-dbus libqt4-network libqt4-xml libasound2
		wget http://www.skype.com/go/getskype-linux-beta-ubuntu-64
		sudo dpkg -i getskype-*
		rm -rf skype-ubuntu-precise*
		cd ~
fi

# Install SmartMonTols to monitor your disks and get info about them... works with SSDs
# http://www.thomas-krenn.com/en/wiki/SMART_tests_with_smartctl
# http://www.smartmontools.org/
if [[ "smatctl" == $command || "all" == $command ]]
	then
		sudo apt-get -y install smartmontools
fi

if [[ "utils" == $command || "all" == $command ]]
	then
		sudo apt-get install unetbootin
		sudo apt-get install gparted
fi

sudo apt-get -f install
