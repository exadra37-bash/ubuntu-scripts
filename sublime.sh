#!/bin/bash
# @author Exadra37 <exadra37@gmail.com>
# @since 2014/11/11
# @link exadra37.com

command="$1"

sudo apt-get -y update

# Install Sublime Text 3 - Very Fast a Powerfull text editor
# http://www.sublimetext.com/3
if [[ "sublime" == $command || "all" == $command ]]
	then
		printf "\n Installing Sublime Text 3 \n"
		cd /tmp
		wget http://c758482.r82.cf2.rackcdn.com/sublime-text_build-3065_amd64.deb
		sudo dpkg -i -R sublime-text_build-3065_amd64.deb
		# symbolic lunk to allow open it from command line
		ln -s /opt/sublime_text/sublime_text /usr/bin/sublime
		rm -rf sublime-text_build-3065_amd64.deb
		cd ~
fi

# http://howto.biapy.com/fr/ubuntu-gnu-linux/developpement/installer-php-codesniffer-sur-ubuntu
if [[ "phpcs" == $command || "all" == $command ]]
	then
		printf "\n Installing Dependencies for Php Code Sniffer \n"
		sudo apt-get -y install php-pear
		sudo bash -c "command pear channel-update pear.php.net && command pear upgrade PEAR"

		printf "\n Installing Php Code Sniffer \n"
		sudo pear install PHP_CodeSniffer
fi

# https://github.com/FriendsOfPHP/PHP-CS-Fixer
if [[ "phpcsfixer" == $command || "all" == $command ]]
	then
		printf "\n Installing Php Code Standard Fixer \n"
		sudo wget http://get.sensiolabs.org/php-cs-fixer.phar -O /usr/local/bin/php-cs-fixer
		sudo chmod a+x /usr/local/bin/php-cs-fixer
fi

# http://stackoverflow.com/a/25028478
if [[ "sheck" == $command || "all" == $command ]]
	then
		printf "\n Installing Php Sheck \n"
		cd /opt/
		sudo git clone --depth=1 https://github.com/facebook/pfff.git
		sudo ./configure
		sudo make depend
		sudo make
		sudo make opt
		cd -
fi

# http://howto.biapy.com/fr/ubuntu-gnu-linux/developpement/installer-php-codesniffer-sur-ubuntu
if [[ "phpmd" == $command || "all" == $command ]]
	then
		printf "\n Installing Dependencies for Php Mess Detector \n"
		
		if [[ "phpmd" == $command ]]
			then
				sudo apt-get -y install php-pear
				sudo bash -c "command pear channel-update pear.php.net && command pear upgrade PEAR"
		fi
		
		sudo apt-get -y install php5-imagick php5-dev build-essential
		sudo pear channel-discover 'pear.phpmd.org'
		sudo pear channel-discover 'pear.pdepend.org'

		printf "\n Installing Php Mess Detector \n"
		sudo pear install --alldeps 'phpmd/PHP_PMD'
fi

sudo apt-get -y -f install

# After install sublime paclkage phpcs, please go to Preferences > Package Settings > Php Code Sniffer > Settings User
# now it will open the file phpcs.sublime-settings, that should be empty and we need to add the following: 
# {
#	  // It seems python/sublime cannot always find the phpcs application
#	  // If empty, then use PATH version of phpcs, else use the set value
#     // In command line type which phpcs to see the path
#     // ➜  ubuntu $: which phpcs
#     //    /usr/bin/phpcs
#	  "phpcs_executable_path": "/usr/bin/phpcs",
#	  
# 	  // Path to where you have the php-cs-fixer installed
#     // In command line type which php-cs-fixer to see the path
#     // ➜  ubuntu $: which php-cs-fixer
#     //    /usr/local/bin/php-cs-fixer
# 	  "php_cs_fixer_executable_path": "/usr/local/bin/php-cs-fixer",
#
# 	  // It seems python/sublime cannot always find the scheck application
#     // If empty, then use PATH version of scheck, else use the set value
#     "scheck_executable_path": "/opt/pfff/scheck",
#
#     // It seems python/sublime cannot always find the phpmd application
#     // If empty, then use PATH version of phpmd, else use the set value
#     // In command line type which phmd to see the path
#     // ➜  ubuntu $: which phpmd
#     //    /usr/bin/phpmd
#     "phpmd_executable_path": "/usr/bin/phpmd",
# }