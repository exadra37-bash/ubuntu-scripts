#!/bin/bash
# @author Exadra37 <exadra37@gmail.com>
# @since 2014/11/11
# @link exadra37.com

command="$1"

sudo apt-get -y update

# http://www.unixmen.com/install-lamp-server-apache-mysql-mariadb-php-ubuntu-14-1014-0413-10/
if [[ "apache" == $command || "all" == $command ]]
	then
		printf "\n Installing Apache \n"
		sudo apt-get -y install apache2
fi

# http://www.unixmen.com/install-lamp-server-apache-mysql-mariadb-php-ubuntu-14-1014-0413-10/
# http://howto.biapy.com/fr/ubuntu-gnu-linux/developpement/installer-php-codesniffer-sur-ubuntu
if [[ "php" == $command || "all" == $command ]]
	then
		printf "\n Installing PHP \n"
		sudo apt-get -y install php5 php5-mysql libapache2-mod-php5 php5-xdebug php-pear
		sudo bash -c "command pear channel-update pear.php.net && command pear upgrade PEAR"
fi

sudo apt-get -y -f install