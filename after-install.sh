#!/bin/bash
# @author Exadra37 <kfoxtrot37@gmail.com>
# @since 2014/10/26
# @link exadra37.com

command="$1"

# 1. Enable Firewall... by default is enable, but just in case it not we will eanable it.
# https://help.ubuntu.com/12.04/serverguide/firewall.html
if [[ "firewall" == $command || "all" == $command ]]
	then
		printf "\n 1. Try To Enable Firewall \n"
		sudo ufw enable
fi

# 2. First we need to update
if [[ "update" == $command || "all" == $command ]]
	then
		printf "\n 2. Updating \n"
		sudo apt-get -y update
fi

# 3. Now we need to install last available upgrades
if [[ "upgrade" == $command || "all" == $command ]]
	then
		printf "\n 3. Upgrading \n"
		sudo apt-get -y upgrade
fi

# 5. Tool for Version Control System.
# http://git-scm.com/
if [[ "git" == $command || "all" == $command ]]
	then
		printf "\n 5. Installing Git Version Control System. \n"
		sudo apt-get -y install git
		git config --global user.email "$email"
		git config --global user.name "$name"
fi

# 6. Track our home directory for all configuration files
if [[ "git-track-home" == $command || "all" == $command ]]
	then
		printf "\n Tracking Home Directory for changes in Config Files \n"
		file=.gitignore
		cd /home/$USER
		echo '# ignore all files and folders' > $file
		echo '*' >> $file
		echo '# except all hidden files and folders' >> $file
		echo '!/.**' >> $file
		git init
		git add --all
		git commit -m 'Initial Configuration.'
fi

# 7. Track /etc folder for changes
if [[ "git-track-etc" == $command || "all" == $command ]]
	then
		printf "\n Tracking ETC Directory for changes in all files \n"
		cd /etc
		git init
		git add --all
		git commit -m 'Initial Configuration.'
fi

# 8. Lets Restore our Privacy
# https://fixubuntu.com/
if [[ "restore-privacy" == $command || "all" == $command ]]
	then
		printf "\n 8. Restoing Privacy \n"
		GS="/usr/bin/gsettings"
		CCUL="com.canonical.Unity.lenses"
		# Check Canonical schema is present. Take first match, ignoring case.
		SCHEMA="`$GS list-schemas | grep -i $CCUL | head -1`"
		if [[ -z "$SCHEMA" ]]
			then
				printf "Error: could not find Canonical schema %s.\n" "$CCUL" 1>&2
			else
				CCUL="$SCHEMA"
				printf "Found Cannonical Schema \n"
				# Turn off "Remote Search", so search terms in Dash don't get sent to the internet
				$GS set $CCUL remote-content-search none
				$GS set $CCUL disabled-scopes \
		      		"['more_suggestions-amazon.scope', 'more_suggestions-u1ms.scope',
		      		'more_suggestions-populartracks.scope', 'music-musicstore.scope',
		     	 	'more_suggestions-ebay.scope', 'more_suggestions-ubuntushop.scope',
		      		'more_suggestions-skimlinks.scope']"

		      	# Block connections to Ubuntu's ad server, just in case
		      	if ! grep -q "127.0.0.1 productsearch.ubuntu.com" /etc/hosts
		      		then
		      			echo -e "\n127.0.0.1 productsearch.ubuntu.com" | sudo tee -a /etc/hosts >/dev/null
		      	fi

		    # remove also Amazon from Unity Dash and Launcher
		    sudo rm -rf /usr/share/applications/ubuntu-amazon-default.desktop 
		fi
fi
